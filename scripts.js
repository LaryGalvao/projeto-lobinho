fetch('https://lobinhos.herokuapp.com/wolves')
    .then(response => response.json())
    .then(data => { //aqui se trabalha com o json
        data.forEach(lobo => {
            const card = document.createElement('div');
            card.setAttribute('class', 'card');

            const h1 = document.createElement('h1');
            h1.textContent = lobo.name;

            const p = document.createElement('p');
            lobo.description = lobo.description.substring(10, 255); // Limite para 255 chars
            p.textContent = `${lobo.description}...`; // Concatenamos com reticências

            // Damos Append dos cartões para o elemento contêiner
            container.appendChild(card);

        // Cada cartão terá seu h1 e p
        card.appendChild(h1);
        card.appendChild(p);
        })
    })
    
    .catch(err =>{ // fazer algo com os erros
        console.log(err)
    });

const app = document.getElementById('root');

//const logo = document.createElement('img');
//logo.src='midias/logo.png'

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);